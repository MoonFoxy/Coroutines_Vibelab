import kotlinx.coroutines.*

@OptIn(DelicateCoroutinesApi::class)
fun main(args: Array<String>) {
    GlobalScope.launch {
        delay(1000L)
        println("World")
    }
    runBlocking {
        delay(2000L)
        println("Hello")
    }
}

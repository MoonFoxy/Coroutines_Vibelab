import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*

fun main(args: Array<String>) = runBlocking {
    val first = async { firstThread() }
    val second = async { secondThread() }
    val result = first.await() + second.await()
    printCurrentTime("Result: $result")
}

suspend fun firstThread(): Int {
	delay(1000L)
    printCurrentTime("World")
    return 1337
}

suspend fun secondThread(): Int {
    delay(2000L)
    printCurrentTime("Hello")
    return 1488
}

fun printCurrentTime(message: String) {
    val time = (SimpleDateFormat("hh:mm:ss")).format(Date())
    println("[$time] $message")
}
